// ==UserScript==
// @name        Twitch :: Channel points auto-clicker
// @description Automatically redeem bonus channel points when prompted
//
// @author      Arno_QS
// @namespace   https://gitlab.com/arno-qs
// @version     1.2.0
// @icon        data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAAA8CAIAAAB+RarbAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAB3RJTUUH4wofFxU5bxdHDQAAAw1JREFUaN7t209MUnEcAPAvL9uAAS75o4P3TMVZCRQXbUKbmgeN4ealhqc8pHapDgy3WtZWcx2cHexkXeoktQ62mtODZQfg4MkJLtio9InlBjSQfB7Y6MAiKxHSH/b+/L7HH4+3ffb7fb+/P+89USaTgZ/xfmN5Nro4E12it2PA/TDISINM56y11Smqco2iHPhWwP1k7R3wMa6QbQ8Mvb+B2zz3AqkI8DcMMnLeOgwARLZv+a0FgEBq7WZgEgBE4cSXs767IIx4ab5xJO1o5H335qK8TEosC0YLADPRRcKfWhMOmN6OEyCwwGAMxmAM5lSUob1dtUQ5buwzKUhFmfSAt0qmt6Y3FkfDr1cZlFs3kWpmAKF2qslJSZQoZ04m1rMwhtCMckgj1wIAJVGOG/vYmMMObQtybTasFQ2WigbWgW0ac+kqjUNrYR2YkqpKBz7Hwh42ysnSgREmC56H9xuTES8nwCjnYby05PWQthxrKLRUZPybdL4lGikuUJa830LsAr9qdhZKct91/9Ndf3Lp7Q5dgZlWPTuIhzQGYzAGswNcfuDTAjaCe3Utu5Zih9ZyodLMzx5+ZLw8cLxjZ4tJTo6cusjnIT1y8pJL353TTjU7FYc4ngH5IZ4nHkqkv1srTuyRlkP1dgB4vu7dW+vfpJeStK3SjDbDEYPdEa973QcADl2LS99dnWcfO1Rvv1pzPp/WEw+Oht944iEAqJaorOh2/yUc0u6Ib+/TxnzaiZW5noWHWS3Hcjh7wppIb/1LRgRvf3jB4aJFM7HHn+eKv/6a/xnnq/TEytsir/Rv0jQT4zw4kd6imWgxV64yh/E2HOIqPVjT8fdyqvxoUfOKSU5NNf25qTYpSFaDjXJq3/+lJMoSPbsoCdgTD3Jit4RPLTEYgzEYgzEYg/8jmBIrhaM1yHREl+q0kMAk0akWENhZayNaNY39ZLsQtP1ke52iKvcZz/0Ar1+NN8h089Y7v6r0vHWYx/3cT7ZntbDzyzQA+Jj8OvZpOpCK8KO3KbGyS2XqVJ9p1TTmGn8AX+f1lQjB/R8AAAAASUVORK5CYII=
//
// @include     https://www.twitch.tv/*
// @include     https://twitch.tv/*
//
// @exclude     https://api.twitch.tv/*
//
// @source      https://gitlab.com/arno-qs/twitch-channel-points-auto-clicker/blob/master/twitch-channel-points-auto-clicker.user.js
// @website     https://gitlab.com/arno-qs/twitch-channel-points-auto-clicker/blob/master/twitch-channel-points-auto-clicker.user.js
// @homepage    https://gitlab.com/arno-qs/twitch-channel-points-auto-clicker/blob/master/twitch-channel-points-auto-clicker.user.js
// @homepageURL https://gitlab.com/arno-qs/twitch-channel-points-auto-clicker/blob/master/twitch-channel-points-auto-clicker.user.js
// @supportURL  https://gitlab.com/arno-qs/twitch-channel-points-auto-clicker/issues
// @downloadURL https://gitlab.com/arno-qs/twitch-channel-points-auto-clicker/raw/master/twitch-channel-points-auto-clicker.user.js
// @installURL  https://gitlab.com/arno-qs/twitch-channel-points-auto-clicker/raw/master/twitch-channel-points-auto-clicker.user.js
// @updateURL   https://gitlab.com/arno-qs/twitch-channel-points-auto-clicker/raw/master/twitch-channel-points-auto-clicker.user.js
//
// @grant       none
// @run-at      document-end
// ==/UserScript==

(function() {

    function initialize_mutation_observer() {

        // Set up the callback function for the mutation observer
        const mutation_observer_callback = function(mutationsList, observer) {
            for ( let mutation of mutationsList ) {
                if ( mutation.type === "childList") {
                    var current_time = new Date().toLocaleString()
                    const bonus_icon = document.getElementsByClassName("claimable-bonus__icon")[0]
                    const point_award_element = document.getElementsByClassName("community-points-summary__points-add-text")[0]
                    if ( bonus_icon ) {
                        console.debug("[" + current_time + "] [Twitch Bonus Clicker] Claiming Twitch channel points")
                        bonus_icon.click()
                    }
                    if ( point_award_element ) {
                        console.debug("[" + current_time + "] [Twitch Bonus Clicker] Points awarded:", document.getElementsByClassName("community-points-summary__points-add-text")[0].innerText)
                    }
                }
            }
        }

        // Create an observer instance linked to the callback function
        const observer = new MutationObserver(mutation_observer_callback)

        // Start observing the target node for configured mutations
        const input_buttons_container = document.getElementsByClassName("chat-input__buttons-container")[0]
        const mutation_observer_config = {
            attributes: false,
            childList: true,
            subtree: true
        }
        observer.observe(input_buttons_container, mutation_observer_config)

        console.info("[Twitch Bonus Clicker] Initialized mutation observer")
        console.debug("[Twitch Bonus Clicker] Input buttons container:", input_buttons_container)
    }

    function one_off_click() {
        // Send a single click event right now in case the button's already active
        var bonusbutton = document.getElementsByClassName("claimable-bonus__icon")[0]
        if ( bonusbutton ) {
            console.debug("[Twitch Bonus Clicker] One-off clicking the bonus button")
            bonusbutton.click()
        }
    }

    var mutation_timeout = window.setTimeout(initialize_mutation_observer, 10000)
    var one_off_timeout = window.setTimeout(one_off_click, 30000)

 })();
