Twitch Channel Points Auto-clicker
==================================

This script watches the page for the "bonus channel points" icon to pop up, and
automatically clicks it.  This way, you can maximize the bonus points you
accrue, even if you've got other streams in the background, you're watching in
full-screen mode, the mouse is out of reach, etc.

It shouldn't require any configuration; you can verify that it's working (or
not working, I suppose) in Firefox at least by pulling up the Web Console,
filtering for "[Twitch Bonus Clicker]" (without quotes), and looking for the
following:

1. When you first (re)load a page, after a few seconds you should see the
   message "Initialized mutation observer".
2. When you get bonus points and the script clicks the button, you'll see
   the message "Claiming Twitch channel points", along with messages about the
   points awarded ("Points awarded: +50").
3. You should also see messages about points being awarded every five minutes
   when you get the automatic points; the script isn't clicking anything for
   that, but it still triggers the "Points awarded" message.

Obviously, you can also just wait about five minutes (that seems to be the
initial delay) and make sure that when the bonus icon shows up, the script
clicks on it automatically.  Since it's a proper DOM mutation observer (and not
just a simple timer), it may react so fast that you don't see the icon...but
you will see the "+50" or whatever when that bubbles up.

Simple version
--------------

For now, this userscript comes in two flavors.  The "official" version is the
one documented above, which uses a proper MutationObserver object to watch for
changes in the page and react accordingly.

The problem with that approach is that it only looks for the claim button once,
so if the DOM gets recreated, that connection gets lost and it's never
reestablished.  In practical terms, this means that if the channel you're
watching does a raid, or if you hide the chat (even if you show it again), the
script won't work until you manually refresh the page.

It's a solvable problem, but requires me to dive back into the details of how
Twitch manipulates the DOM, and I wanted a quick fix so I don't have to suffer
while I'm working on it.  That's what the simple version is.  There's no
MutationObserver, it just looks for the button on a timer (5 seconds, by
default) and if it's there, it clicks it.  Low-tech, but effective.

There's no reason to install/enable both at once, but if you do enable both at
once it shouldn't hurt anything; they should both just do their thing
independently.  I suppose "best practice" would be to install both, so you're
set up for updates, and just disable the "official" one until I fix it so it
follows the button through Twitch's DOM changes.
