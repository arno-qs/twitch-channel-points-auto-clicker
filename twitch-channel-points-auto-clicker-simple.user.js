// ==UserScript==
// @name        Twitch :: Channel points auto-clicker (simple)
// @description Automatically redeem bonus channel points periodically (simple version)
//
// @author      Arno_QS
// @namespace   https://gitlab.com/arno-qs
// @version     1.0.0
// @icon        data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAAA8CAIAAAB+RarbAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAB3RJTUUH4wofFxU5bxdHDQAAAw1JREFUaN7t209MUnEcAPAvL9uAAS75o4P3TMVZCRQXbUKbmgeN4ealhqc8pHapDgy3WtZWcx2cHexkXeoktQ62mtODZQfg4MkJLtio9InlBjSQfB7Y6MAiKxHSH/b+/L7HH4+3ffb7fb+/P+89USaTgZ/xfmN5Nro4E12it2PA/TDISINM56y11Smqco2iHPhWwP1k7R3wMa6QbQ8Mvb+B2zz3AqkI8DcMMnLeOgwARLZv+a0FgEBq7WZgEgBE4cSXs767IIx4ab5xJO1o5H335qK8TEosC0YLADPRRcKfWhMOmN6OEyCwwGAMxmAM5lSUob1dtUQ5buwzKUhFmfSAt0qmt6Y3FkfDr1cZlFs3kWpmAKF2qslJSZQoZ04m1rMwhtCMckgj1wIAJVGOG/vYmMMObQtybTasFQ2WigbWgW0ac+kqjUNrYR2YkqpKBz7Hwh42ysnSgREmC56H9xuTES8nwCjnYby05PWQthxrKLRUZPybdL4lGikuUJa830LsAr9qdhZKct91/9Ndf3Lp7Q5dgZlWPTuIhzQGYzAGswNcfuDTAjaCe3Utu5Zih9ZyodLMzx5+ZLw8cLxjZ4tJTo6cusjnIT1y8pJL353TTjU7FYc4ngH5IZ4nHkqkv1srTuyRlkP1dgB4vu7dW+vfpJeStK3SjDbDEYPdEa973QcADl2LS99dnWcfO1Rvv1pzPp/WEw+Oht944iEAqJaorOh2/yUc0u6Ib+/TxnzaiZW5noWHWS3Hcjh7wppIb/1LRgRvf3jB4aJFM7HHn+eKv/6a/xnnq/TEytsir/Rv0jQT4zw4kd6imWgxV64yh/E2HOIqPVjT8fdyqvxoUfOKSU5NNf25qTYpSFaDjXJq3/+lJMoSPbsoCdgTD3Jit4RPLTEYgzEYgzEYg/8jmBIrhaM1yHREl+q0kMAk0akWENhZayNaNY39ZLsQtP1ke52iKvcZz/0Ar1+NN8h089Y7v6r0vHWYx/3cT7ZntbDzyzQA+Jj8OvZpOpCK8KO3KbGyS2XqVJ9p1TTmGn8AX+f1lQjB/R8AAAAASUVORK5CYII=
//
// @include     https://www.twitch.tv/*
// @include     https://twitch.tv/*
//
// @exclude     https://api.twitch.tv/*
//
// @source      https://gitlab.com/arno-qs/twitch-channel-points-auto-clicker/blob/master/twitch-channel-points-auto-clicker-simple.user.js
// @website     https://gitlab.com/arno-qs/twitch-channel-points-auto-clicker/blob/master/twitch-channel-points-auto-clicker-simple.user.js
// @homepage    https://gitlab.com/arno-qs/twitch-channel-points-auto-clicker/blob/master/twitch-channel-points-auto-clicker-simple.user.js
// @homepageURL https://gitlab.com/arno-qs/twitch-channel-points-auto-clicker/blob/master/twitch-channel-points-auto-clicker-simple.user.js
// @supportURL  https://gitlab.com/arno-qs/twitch-channel-points-auto-clicker/issues
// @downloadURL https://gitlab.com/arno-qs/twitch-channel-points-auto-clicker/raw/master/twitch-channel-points-auto-clicker-simple.user.js
// @installURL  https://gitlab.com/arno-qs/twitch-channel-points-auto-clicker/raw/master/twitch-channel-points-auto-clicker-simple.user.js
// @updateURL   https://gitlab.com/arno-qs/twitch-channel-points-auto-clicker/raw/master/twitch-channel-points-auto-clicker-simple.user.js
//
// @grant       none
// @run-at      document-end
// ==/UserScript==

// You can increase this if the resource usage actually becomes a problem
var seconds_between_clicks = 5

///////////////////////////////////////////////////////////////////////////////

function click_points_button() {
    var points_button = document.getElementsByClassName("claimable-bonus__icon")[0]
    if (typeof points_button !== 'undefined') {
        console.log("Simple Twitch channel points redemption click in interval ID " + interval_id)
        points_button.click()
    }
}

var interval_id = window.setInterval(click_points_button, seconds_between_clicks * 1000)
console.log("Interval ID:", interval_id)

// EOF
////////
